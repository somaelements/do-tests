(function(){
  function searchWikipedia(term) {
    var url = 'http://pt.wikipedia.org/w/api.php?action=opensearch&format=json&search='
      + encodeURIComponent(term) + '&callback=JSONPCallback';
    return Rx.DOM.jsonpRequest(url);
  }

  function clearSelector (element) {
    while (element.firstChild) {
      element.removeChild(element.firstChild);
    }
  }

  function createLineItem(text) {
      var li = document.createElement('li');
      li.innerHTML = text;
      return li;
  }

  var textInput = document.getElementById('textInput');
  console.log('Início');
  function doIt() {
    var throttledInput;
    // TODO: ver como podemos garantir que Rx.DOM já esteja carregado
    (function(){
      throttledInput = Rx.DOM.keyup(textInput)
        .pluck('target','value')
        .filter( function (text) {
            return text.length > 2;
        })
        .debounce(250)
        .distinctUntilChanged();
      var suggestions = throttledInput.flatMapLatest(searchWikipedia);
      var resultList = document.getElementById('results');

      suggestions.subscribe(function (data) {
        var results = data.response[1];

        clearSelector(resultList);

        for (var i = 0; i < results.length; i++) {
          resultList.appendChild(createLineItem(results[i]));
        }
      },
      function (e) {
        clearSelector(resultList);
        resultList.appendChild(createLineItem('Error: ' + e));
      });
    })();
  }
  // Devemos aguardar o Load do Rx
  // TODO: ver forma alternativa para garantir que Rx.DOM já esteja carregado
  console.log('typeof Rx === "' + (typeof Rx) + '"');
  ensureRxDOMIsReady(function(){
    doIt();
  });
})();
